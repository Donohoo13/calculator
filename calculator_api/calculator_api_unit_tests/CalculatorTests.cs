using System;
using calculator_api.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace calculator_api_unit_tests
{
    [TestClass]
    public class CalculatorTests
    {
        [TestMethod]
        public void GetOperations_ReturnsArrayOfOperations()
        {
            var calc = new Calculator();
            var operations = calc.GetOperations();

            Assert.AreEqual(operations.Length, 4);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Calculate_InvalidOperation_ThrowsArgumentException()
        {
            var calc = new Calculator();
            calc.Calculate(3, 2, ':');
        }

        [TestMethod]
        public void Calculate_Add_ShouldReturnAddition()
        {
            var calc = new Calculator();
            var result = calc.Calculate(3, 2, '+');

            Assert.AreEqual(result, 5);
        }

        [TestMethod]
        public void Calculate_Subtract_ShouldReturnSubtraction()
        {
            var calc = new Calculator();
            var result = calc.Calculate(10, 4, '-');

            Assert.AreEqual(result, 6);
        }

        [TestMethod]
        public void Calculate_Divide_ShouldReturnDivision()
        {
            var calc = new Calculator();
            var result = calc.Calculate(50, 2, '/');

            Assert.AreEqual(result, 25);
        }

        [TestMethod]
        public void Calculate_Multiply_ShouldReturnMultiplication()
        {
            var calc = new Calculator();
            var result = calc.Calculate(8, 4, '*');

            Assert.AreEqual(result, 32);
        }
    }
}
