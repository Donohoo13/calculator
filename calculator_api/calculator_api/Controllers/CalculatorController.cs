﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using calculator_api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace calculator_api.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class CalculatorController : ControllerBase
    {
        private readonly ICalculator _calculator;

        public CalculatorController(ICalculator calculator)
        {
            _calculator = calculator;
        }

        [HttpGet]
        public Operation[] Get()
        {
            var operations = _calculator.GetOperations();

            return operations;
        }

        [HttpPost("calculate")]
        public int Get(CalculateRequest cr)
        {
            return _calculator.Calculate(cr.X, cr.Y, cr.Operation);
        }
    }
}
