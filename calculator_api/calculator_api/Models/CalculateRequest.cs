﻿using System;
namespace calculator_api.Models
{
    public class CalculateRequest
    {
        public int X { get; set; }

        public int Y { get; set; }

        public char Operation { get; set; }
    }
}
