﻿using System;
namespace calculator_api.Models
{
    public interface ICalculator
    {
        Operation[] GetOperations();

        int Calculate(int x, int y, char operation);
    }

    public class Calculator : ICalculator
    {
        public Operation[] GetOperations()
        {
            Operation[] operations = { new Operation('+', "Add"), new Operation('-', "Subtract"), new Operation('/', "Divide"), new Operation('*', "Multiply") };

            return operations;
        }

        public int Calculate(int x, int y, char operation)
        {
            switch (operation)
            {
                case '+':
                    return x + y;

                case '-':
                    return x - y;

                case '/':
                    decimal result = x / y;
                    return (int)Math.Round(result, 0);

                case '*':
                    return x * y;

                default:
                    throw new ArgumentException($"{operation} is not a valid operation method, please pick another method and try again.");
            }
        }
    }
}
