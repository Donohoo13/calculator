﻿using System;
namespace calculator_api.Models
{
    public class Operation
    {
        public char Quantifier { get; set; }

        public string Name { get; set; }

        public Operation(char quantifier, string name)
        {
            this.Quantifier = quantifier;

            this.Name = name;
        }
    }
}
