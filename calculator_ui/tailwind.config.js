module.exports = {
	purge: ['./src/**/*.{html,ts}'],
	darkMode: false, // or 'media' or 'class'
	theme: {
		fontFamily: {
			mono: ['ui-monospace', 'SFMono-Regular']
		},
		container: {
			center: true
		},
		extend: {}
	},
	variants: {
		extend: {}
	},
	plugins: [require('@tailwindcss/typography'), require('@tailwindcss/forms')]
};
