import { Operation } from './classes/operation';
import { CalculatorService } from './app.service';
import { Component } from '@angular/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {
	title: string = 'calculator';
	operation: string = '';
	availableOperations: Operation[];
	result: number;

	constructor(private service: CalculatorService) {}

	getOperations() {
		this.service.getOperations().subscribe((res) => (this.availableOperations = res));
	}

	onSubmit(data, operator) {
		this.operation = operator.quantifier;
		this.service.runCalculation(data, operator).subscribe((res) => (this.result = res));
	}
}
