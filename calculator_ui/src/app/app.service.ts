import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import { Operation } from './classes/operation';

@Injectable()
export class CalculatorService {
	constructor(private http: HttpClient) {}

	getOperations(): Observable<Operation[]> {
		const result = this.http.get<Operation[]>('https://localhost:5001/Calculator');
		return result;
	}

	runCalculation(formData, operator): Observable<number> {
		const data = { x: formData.inputX, y: formData.inputY, operation: operator.quantifier };

		let headers = new HttpHeaders({
			'Content-Type': 'application/json'
		});
		let options = { headers: headers };

		const result = this.http.post<number>('https://localhost:5001/Calculator/calculate', data, options);
		return result;
	}
}
